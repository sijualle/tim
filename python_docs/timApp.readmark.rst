timApp.readmark package
=======================

Submodules
----------

timApp.readmark.readings module
-------------------------------

.. automodule:: timApp.readmark.readings
    :members:
    :undoc-members:
    :show-inheritance:

timApp.readmark.readmarkcollection module
-----------------------------------------

.. automodule:: timApp.readmark.readmarkcollection
    :members:
    :undoc-members:
    :show-inheritance:

timApp.readmark.readparagraph module
------------------------------------

.. automodule:: timApp.readmark.readparagraph
    :members:
    :undoc-members:
    :show-inheritance:

timApp.readmark.readparagraphtype module
----------------------------------------

.. automodule:: timApp.readmark.readparagraphtype
    :members:
    :undoc-members:
    :show-inheritance:

timApp.readmark.routes module
-----------------------------

.. automodule:: timApp.readmark.routes
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.readmark
    :members:
    :undoc-members:
    :show-inheritance:
