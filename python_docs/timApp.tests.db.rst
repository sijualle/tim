timApp.tests.db package
=======================

Submodules
----------

timApp.tests.db.test\_answers module
------------------------------------

.. automodule:: timApp.tests.db.test_answers
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_clipboard module
--------------------------------------

.. automodule:: timApp.tests.db.test_clipboard
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_create\_db module
---------------------------------------

.. automodule:: timApp.tests.db.test_create_db
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_document module
-------------------------------------

.. automodule:: timApp.tests.db.test_document
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_folder\_rename module
-------------------------------------------

.. automodule:: timApp.tests.db.test_folder_rename
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_import\_accounts module
---------------------------------------------

.. automodule:: timApp.tests.db.test_import_accounts
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_item\_create module
-----------------------------------------

.. automodule:: timApp.tests.db.test_item_create
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_markdownconverter module
----------------------------------------------

.. automodule:: timApp.tests.db.test_markdownconverter
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_notify module
-----------------------------------

.. automodule:: timApp.tests.db.test_notify
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_personal\_folder module
---------------------------------------------

.. automodule:: timApp.tests.db.test_personal_folder
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_plugin module
-----------------------------------

.. automodule:: timApp.tests.db.test_plugin
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_references module
---------------------------------------

.. automodule:: timApp.tests.db.test_references
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_settings module
-------------------------------------

.. automodule:: timApp.tests.db.test_settings
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.test\_users module
----------------------------------

.. automodule:: timApp.tests.db.test_users
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.db.timdbtest module
--------------------------------

.. automodule:: timApp.tests.db.timdbtest
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.tests.db
    :members:
    :undoc-members:
    :show-inheritance:
