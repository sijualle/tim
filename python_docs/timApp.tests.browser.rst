timApp.tests.browser package
============================

Submodules
----------

timApp.tests.browser.browsertest module
---------------------------------------

.. automodule:: timApp.tests.browser.browsertest
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.browser.test\_answerbrowser module
-----------------------------------------------

.. automodule:: timApp.tests.browser.test_answerbrowser
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.browser.test\_csplugin module
------------------------------------------

.. automodule:: timApp.tests.browser.test_csplugin
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.browser.test\_login module
---------------------------------------

.. automodule:: timApp.tests.browser.test_login
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.browser.test\_pareditor module
-------------------------------------------

.. automodule:: timApp.tests.browser.test_pareditor
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.browser.test\_questions module
-------------------------------------------

.. automodule:: timApp.tests.browser.test_questions
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.browser.test\_timtable module
------------------------------------------

.. automodule:: timApp.tests.browser.test_timtable
    :members:
    :undoc-members:
    :show-inheritance:

timApp.tests.browser.test\_velps module
---------------------------------------

.. automodule:: timApp.tests.browser.test_velps
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.tests.browser
    :members:
    :undoc-members:
    :show-inheritance:
