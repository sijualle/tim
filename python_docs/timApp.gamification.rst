timApp.gamification package
===========================

Submodules
----------

timApp.gamification.docgamified module
--------------------------------------

.. automodule:: timApp.gamification.docgamified
    :members:
    :undoc-members:
    :show-inheritance:

timApp.gamification.documentgamificationpoint module
----------------------------------------------------

.. automodule:: timApp.gamification.documentgamificationpoint
    :members:
    :undoc-members:
    :show-inheritance:

timApp.gamification.gamificationdata module
-------------------------------------------

.. automodule:: timApp.gamification.gamificationdata
    :members:
    :undoc-members:
    :show-inheritance:

timApp.gamification.gamificationdocument module
-----------------------------------------------

.. automodule:: timApp.gamification.gamificationdocument
    :members:
    :undoc-members:
    :show-inheritance:

timApp.gamification.gamificationdocumenttype module
---------------------------------------------------

.. automodule:: timApp.gamification.gamificationdocumenttype
    :members:
    :undoc-members:
    :show-inheritance:

timApp.gamification.gamificationpointtype module
------------------------------------------------

.. automodule:: timApp.gamification.gamificationpointtype
    :members:
    :undoc-members:
    :show-inheritance:

timApp.gamification.generateMap module
--------------------------------------

.. automodule:: timApp.gamification.generateMap
    :members:
    :undoc-members:
    :show-inheritance:

timApp.gamification.usergamification module
-------------------------------------------

.. automodule:: timApp.gamification.usergamification
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.gamification
    :members:
    :undoc-members:
    :show-inheritance:
