timApp.document.minutes package
===============================

Submodules
----------

timApp.document.minutes.routes module
-------------------------------------

.. automodule:: timApp.document.minutes.routes
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: timApp.document.minutes
    :members:
    :undoc-members:
    :show-inheritance:
